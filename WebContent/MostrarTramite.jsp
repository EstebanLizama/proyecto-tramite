<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse ;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="background-color:powderblue;">
<form action="MostrarTramite" method="post">
MOSTRAR FORMULARIO
<br>
<br>
Presione "mostrar" para mostrar los formularios 
<br>


<table>
  <tr>
  <th>Codigo formulario</th>
    <th>Nombre municipalidad</th>
    <th>Ciudad</th>
    <th>Fecha emision</th>
    <th>Tipo</th>
    <th>Nombre cliente</th>
  <th>Apellido cliente</th>
  <th>Rut cliente</th>
  <th>Rut funcionario</th>
  <th>Cargo funcionario</th>
  <th>Descripcion</th>
  </tr>
   <i:forEach items="${listaTramite}" var="tramite">
  <tr>
 
<td>${tramite.codigoformulario} </td> 
<td>${tramite.nombremunicipalidad}</td>
<td>${tramite.ciudad}</td>
<td>${tramite.fechaemision}</td>
<td>${tramite.tipo} </td>
<td>${tramite.nombreusuario}</td>
<td>${tramite.apellidousuario}</td>
<td>${tramite.rutusuario}</td>
<td>${tramite.rutfuncionario}</td>
<td>${tramite.cargofuncionario }</td>
<td>${tramite.descripcion}</td>


  </tr>
 </i:forEach>
 
  
</table>

<br>

<br>
<input type="submit" value="mostrar" >
<br>
<br>
<a href="http://localhost:8080/Tramite/MenuTramite.jsp">Volver menu</a> <> <a href="http://localhost:8080/Tramite/BuscarTramites.jsp"> Buscar formulario por rut</a>
</form>

</body>
</html>