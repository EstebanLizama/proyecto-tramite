<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<body style="background-color:powderblue;">
<form action="ModificarTramite" method="post">
MODIFICAR FORMULARIO
<br>
<br> 
Ingrese datos solicitados
<br>
<br>
<br>
Ingrese el codigo de formulario que desea modificar 
<input type="text"  name="codigonuevo" pattern="[0-9]*" required >

<br>
<br>

Ingresar nuevo formulario 
<br>
Ingrese el nombre de municipalidad
<input type="text"  name=nombremunicipalidad pattern="[a-ZA-Z]*" required >
<br>
Ingrese ciudad 
<input type="text" name="ciudad" pattern="[a-ZA-Z]*" required >
<br>
Ingrese fecha de emision (formato DD-MM-AA)
<input type="text" name="fechaemision" pattern="^\d{2}-\d{2}-\d{2}$" required >
<br>
Ingrese rut funcionario
<input type="text" name="rutfuncionario" pattern="[0-9]{2}[.][0-9]{3}[.][0-9]{3}[-][0-9k]" required >
<br>
Ingrese cargo funcionario
<input type="text" name="cargofuncionario" pattern="[a-ZA-Z]*" required >
<br>
Ingrese el tipo de tramite
<br><input type="radio" name="tipo" value="Patente Municipal">Patente Municipal
<br><input type="radio" name="tipo" value="Permiso Circulacion">Permiso Circulacion
<br><input type="radio" name="tipo" value="Licencia Conducir">Licencia Conducir
<br>
Ingrese el nombre ciudadano
<input type="text" name="nombreusuario" pattern="[a-ZA-Z]*" required >
<br>
Ingrese el apellido ciudadano
<input type="text" name="apellidousuario" pattern="[a-ZA-Z]*" required >
<br>
Ingrese el rut ciudadano
<input type="text" name="rutusuario" pattern="[0-9]{2}[.][0-9]{3}[.][0-9]{3}[-][0-9k]" required >
<br>
Descripcion 
<br>
<textarea name="descripcion" rows="20" cols="60"></textarea>
<br>
${out }
<input type="submit" value="Modificar" >
<br>
<br>
<a href="http://localhost:8080/Tramite/MenuTramite.jsp">Volver menu</a>
</form>
</body>
</html>