
/**
 * ServicioTramiteCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.1  Built on : Aug 31, 2011 (12:22:40 CEST)
 */

    package webservice;

    /**
     *  ServicioTramiteCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServicioTramiteCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServicioTramiteCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServicioTramiteCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for buscarTramite method
            * override this method for handling normal response from buscarTramite operation
            */
           public void receiveResultbuscarTramite(
                    webservice.ServicioTramiteStub.BuscarTramiteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from buscarTramite operation
           */
            public void receiveErrorbuscarTramite(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for mostrarTramite method
            * override this method for handling normal response from mostrarTramite operation
            */
           public void receiveResultmostrarTramite(
                    webservice.ServicioTramiteStub.MostrarTramiteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from mostrarTramite operation
           */
            public void receiveErrormostrarTramite(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for agregarTramite method
            * override this method for handling normal response from agregarTramite operation
            */
           public void receiveResultagregarTramite(
                    webservice.ServicioTramiteStub.AgregarTramiteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from agregarTramite operation
           */
            public void receiveErroragregarTramite(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for modificarTramite method
            * override this method for handling normal response from modificarTramite operation
            */
           public void receiveResultmodificarTramite(
                    webservice.ServicioTramiteStub.ModificarTramiteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from modificarTramite operation
           */
            public void receiveErrormodificarTramite(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for eliminarTramite method
            * override this method for handling normal response from eliminarTramite operation
            */
           public void receiveResulteliminarTramite(
                    webservice.ServicioTramiteStub.EliminarTramiteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from eliminarTramite operation
           */
            public void receiveErroreliminarTramite(java.lang.Exception e) {
            }
                


    }
    