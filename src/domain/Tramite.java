package domain;

public class Tramite {
    private int codigoformulario;
	
	private String fechaemision;
	
	private String ciudad;
	
	private String tipo;
	
	private String descripcion;
	
	private String nombremunicipalidad;
	
	private String nombreusuario;
	
	private String apellidousuario;
	
	private String rutusuario;
	
	private String rutfuncionario;
	
	private String cargofuncionario;
    public Tramite (){}
	public Tramite(int codigoformulario, String fechaemision, String ciudad, String tipo, String descripcion,
			String nombremunicipalidad, String nombreusuario, String apellidousuario, String rutusuario,
			String rutfuncionario, String cargofuncionario) {
		super();
		this.codigoformulario = codigoformulario;
		this.fechaemision = fechaemision;
		this.ciudad = ciudad;
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.nombremunicipalidad = nombremunicipalidad;
		this.nombreusuario = nombreusuario;
		this.apellidousuario = apellidousuario;
		this.rutusuario = rutusuario;
		this.rutfuncionario = rutfuncionario;
		this.cargofuncionario = cargofuncionario;
	}

	public int getCodigoformulario() {
		return codigoformulario;
	}

	public void setCodigoformulario(int codigoformulario) {
		this.codigoformulario = codigoformulario;
	}

	public String getFechaemision() {
		return fechaemision;
	}

	public void setFechaemision(String fechaemision) {
		this.fechaemision = fechaemision;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombremunicipalidad() {
		return nombremunicipalidad;
	}

	public void setNombremunicipalidad(String nombremunicipalidad) {
		this.nombremunicipalidad = nombremunicipalidad;
	}

	public String getNombreusuario() {
		return nombreusuario;
	}

	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}

	public String getApellidousuario() {
		return apellidousuario;
	}

	public void setApellidousuario(String apellidousuario) {
		this.apellidousuario = apellidousuario;
	}

	public String getRutusuario() {
		return rutusuario;
	}

	public void setRutusuario(String rutusuario) {
		this.rutusuario = rutusuario;
	}

	public String getRutfuncionario() {
		return rutfuncionario;
	}

	public void setRutfuncionario(String rutfuncionario) {
		this.rutfuncionario = rutfuncionario;
	}

	public String getCargofuncionario() {
		return cargofuncionario;
	}

	public void setCargofuncionario(String cargofuncionario) {
		this.cargofuncionario = cargofuncionario;
	}
	
}
