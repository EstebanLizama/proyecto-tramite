/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package tramitemunicipal;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TramitesCriteria extends AbstractORMCriteria {
	public final IntegerExpression codigoformulario;
	public final StringExpression fechaemision;
	public final StringExpression ciudad;
	public final StringExpression tipo;
	public final StringExpression descripcion;
	public final StringExpression nombremunicipalidad;
	public final StringExpression nombreusuario;
	public final StringExpression apellidousuario;
	public final StringExpression rutusuario;
	public final StringExpression rutfuncionario;
	public final StringExpression cargofuncionario;
	
	public TramitesCriteria(Criteria criteria) {
		super(criteria);
		codigoformulario = new IntegerExpression("codigoformulario", this);
		fechaemision = new StringExpression("fechaemision", this);
		ciudad = new StringExpression("ciudad", this);
		tipo = new StringExpression("tipo", this);
		descripcion = new StringExpression("descripcion", this);
		nombremunicipalidad = new StringExpression("nombremunicipalidad", this);
		nombreusuario = new StringExpression("nombreusuario", this);
		apellidousuario = new StringExpression("apellidousuario", this);
		rutusuario = new StringExpression("rutusuario", this);
		rutfuncionario = new StringExpression("rutfuncionario", this);
		cargofuncionario = new StringExpression("cargofuncionario", this);
	}
	
	public TramitesCriteria(PersistentSession session) {
		this(session.createCriteria(Tramites.class));
	}
	
	public TramitesCriteria() throws PersistentException {
		this(tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession());
	}
	
	public Tramites uniqueTramites() {
		return (Tramites) super.uniqueResult();
	}
	
	public Tramites[] listTramites() {
		java.util.List list = super.list();
		return (Tramites[]) list.toArray(new Tramites[list.size()]);
	}
}

