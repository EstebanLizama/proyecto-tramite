/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package tramitemunicipal;

public class Tramites {
	public Tramites() {
	}
	
	private int codigoformulario;
	
	private String fechaemision;
	
	private String ciudad;
	
	private String tipo;
	
	private String descripcion;
	
	private String nombremunicipalidad;
	
	private String nombreusuario;
	
	private String apellidousuario;
	
	private String rutusuario;
	
	private String rutfuncionario;
	
	private String cargofuncionario;
	
	private void setCodigoformulario(int value) {
		this.codigoformulario = value;
	}
	
	public int getCodigoformulario() {
		return codigoformulario;
	}
	
	public int getORMID() {
		return getCodigoformulario();
	}
	
	public void setFechaemision(String value) {
		this.fechaemision = value;
	}
	
	public String getFechaemision() {
		return fechaemision;
	}
	
	public void setCiudad(String value) {
		this.ciudad = value;
	}
	
	public String getCiudad() {
		return ciudad;
	}
	
	public void setTipo(String value) {
		this.tipo = value;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setNombremunicipalidad(String value) {
		this.nombremunicipalidad = value;
	}
	
	public String getNombremunicipalidad() {
		return nombremunicipalidad;
	}
	
	public void setNombreusuario(String value) {
		this.nombreusuario = value;
	}
	
	public String getNombreusuario() {
		return nombreusuario;
	}
	
	public void setApellidousuario(String value) {
		this.apellidousuario = value;
	}
	
	public String getApellidousuario() {
		return apellidousuario;
	}
	
	public void setRutusuario(String value) {
		this.rutusuario = value;
	}
	
	public String getRutusuario() {
		return rutusuario;
	}
	
	public void setRutfuncionario(String value) {
		this.rutfuncionario = value;
	}
	
	public String getRutfuncionario() {
		return rutfuncionario;
	}
	
	public void setCargofuncionario(String value) {
		this.cargofuncionario = value;
	}
	
	public String getCargofuncionario() {
		return cargofuncionario;
	}
	
	public String toString() {
		return String.valueOf(getCodigoformulario());
	}
	
}
