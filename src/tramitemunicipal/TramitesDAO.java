/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package tramitemunicipal;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TramitesDAO {
	public static Tramites loadTramitesByORMID(int codigoformulario) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return loadTramitesByORMID(session, codigoformulario);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites getTramitesByORMID(int codigoformulario) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return getTramitesByORMID(session, codigoformulario);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByORMID(int codigoformulario, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return loadTramitesByORMID(session, codigoformulario, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites getTramitesByORMID(int codigoformulario, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return getTramitesByORMID(session, codigoformulario, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByORMID(PersistentSession session, int codigoformulario) throws PersistentException {
		try {
			return (Tramites) session.load(tramitemunicipal.Tramites.class, new Integer(codigoformulario));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites getTramitesByORMID(PersistentSession session, int codigoformulario) throws PersistentException {
		try {
			return (Tramites) session.get(tramitemunicipal.Tramites.class, new Integer(codigoformulario));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByORMID(PersistentSession session, int codigoformulario, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tramites) session.load(tramitemunicipal.Tramites.class, new Integer(codigoformulario), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites getTramitesByORMID(PersistentSession session, int codigoformulario, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tramites) session.get(tramitemunicipal.Tramites.class, new Integer(codigoformulario), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTramites(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return queryTramites(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTramites(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return queryTramites(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites[] listTramitesByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return listTramitesByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites[] listTramitesByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return listTramitesByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTramites(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From tramitemunicipal.Tramites as Tramites");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTramites(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From tramitemunicipal.Tramites as Tramites");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tramites", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites[] listTramitesByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTramites(session, condition, orderBy);
			return (Tramites[]) list.toArray(new Tramites[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites[] listTramitesByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTramites(session, condition, orderBy, lockMode);
			return (Tramites[]) list.toArray(new Tramites[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return loadTramitesByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return loadTramitesByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Tramites[] tramiteses = listTramitesByQuery(session, condition, orderBy);
		if (tramiteses != null && tramiteses.length > 0)
			return tramiteses[0];
		else
			return null;
	}
	
	public static Tramites loadTramitesByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Tramites[] tramiteses = listTramitesByQuery(session, condition, orderBy, lockMode);
		if (tramiteses != null && tramiteses.length > 0)
			return tramiteses[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTramitesByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return iterateTramitesByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTramitesByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession();
			return iterateTramitesByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTramitesByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From tramitemunicipal.Tramites as Tramites");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTramitesByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From tramitemunicipal.Tramites as Tramites");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tramites", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites createTramites() {
		return new tramitemunicipal.Tramites();
	}
	
	public static boolean save(tramitemunicipal.Tramites tramites) throws PersistentException {
		try {
			tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().saveObject(tramites);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(tramitemunicipal.Tramites tramites) throws PersistentException {
		try {
			tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().deleteObject(tramites);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(tramitemunicipal.Tramites tramites) throws PersistentException {
		try {
			tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession().refresh(tramites);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(tramitemunicipal.Tramites tramites) throws PersistentException {
		try {
			tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession().evict(tramites);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tramites loadTramitesByCriteria(TramitesCriteria tramitesCriteria) {
		Tramites[] tramiteses = listTramitesByCriteria(tramitesCriteria);
		if(tramiteses == null || tramiteses.length == 0) {
			return null;
		}
		return tramiteses[0];
	}
	
	public static Tramites[] listTramitesByCriteria(TramitesCriteria tramitesCriteria) {
		return tramitesCriteria.listTramites();
	}
}
