/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package tramitemunicipal;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TramitesDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression codigoformulario;
	public final StringExpression fechaemision;
	public final StringExpression ciudad;
	public final StringExpression tipo;
	public final StringExpression descripcion;
	public final StringExpression nombremunicipalidad;
	public final StringExpression nombreusuario;
	public final StringExpression apellidousuario;
	public final StringExpression rutusuario;
	public final StringExpression rutfuncionario;
	public final StringExpression cargofuncionario;
	
	public TramitesDetachedCriteria() {
		super(tramitemunicipal.Tramites.class, tramitemunicipal.TramitesCriteria.class);
		codigoformulario = new IntegerExpression("codigoformulario", this.getDetachedCriteria());
		fechaemision = new StringExpression("fechaemision", this.getDetachedCriteria());
		ciudad = new StringExpression("ciudad", this.getDetachedCriteria());
		tipo = new StringExpression("tipo", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		nombremunicipalidad = new StringExpression("nombremunicipalidad", this.getDetachedCriteria());
		nombreusuario = new StringExpression("nombreusuario", this.getDetachedCriteria());
		apellidousuario = new StringExpression("apellidousuario", this.getDetachedCriteria());
		rutusuario = new StringExpression("rutusuario", this.getDetachedCriteria());
		rutfuncionario = new StringExpression("rutfuncionario", this.getDetachedCriteria());
		cargofuncionario = new StringExpression("cargofuncionario", this.getDetachedCriteria());
	}
	
	public TramitesDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, tramitemunicipal.TramitesCriteria.class);
		codigoformulario = new IntegerExpression("codigoformulario", this.getDetachedCriteria());
		fechaemision = new StringExpression("fechaemision", this.getDetachedCriteria());
		ciudad = new StringExpression("ciudad", this.getDetachedCriteria());
		tipo = new StringExpression("tipo", this.getDetachedCriteria());
		descripcion = new StringExpression("descripcion", this.getDetachedCriteria());
		nombremunicipalidad = new StringExpression("nombremunicipalidad", this.getDetachedCriteria());
		nombreusuario = new StringExpression("nombreusuario", this.getDetachedCriteria());
		apellidousuario = new StringExpression("apellidousuario", this.getDetachedCriteria());
		rutusuario = new StringExpression("rutusuario", this.getDetachedCriteria());
		rutfuncionario = new StringExpression("rutfuncionario", this.getDetachedCriteria());
		cargofuncionario = new StringExpression("cargofuncionario", this.getDetachedCriteria());
	}
	
	public Tramites uniqueTramites(PersistentSession session) {
		return (Tramites) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Tramites[] listTramites(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Tramites[]) list.toArray(new Tramites[list.size()]);
	}
}

