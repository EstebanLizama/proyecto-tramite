package webservice;

import java.util.ArrayList;
import java.util.List;
import tramitemunicipal.*;
import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Parameter;

import sun.nio.cs.ext.ISCII91;

public class ServicioTramite {
	/**
	* Ingreso de papel tramite
	* @param tramiteU
	* @return
	*/
	public String agregarTramite(domain.Tramite tramiteU){
	PersistentTransaction t;
	try {
	t =
	tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession().beginTransaction();
	try {
	tramitemunicipal.Tramites lormTramite = tramitemunicipal.TramitesDAO.createTramites();
	// Initialize the properties of the persistent object here

	//Input the string for validation
       
	lormTramite.setNombremunicipalidad(tramiteU.getNombremunicipalidad());
	lormTramite.setCiudad(tramiteU.getCiudad());
	lormTramite.setFechaemision(tramiteU.getFechaemision());
	lormTramite.setRutfuncionario(tramiteU.getRutfuncionario());
	lormTramite.setCargofuncionario(tramiteU.getCargofuncionario());
	lormTramite.setTipo(tramiteU.getTipo());
	lormTramite.setNombreusuario(tramiteU.getNombreusuario());
	lormTramite.setApellidousuario(tramiteU.getApellidousuario());
	lormTramite.setRutusuario(tramiteU.getRutusuario());
	lormTramite.setDescripcion(tramiteU.getDescripcion());
	
	System.out.println("Ingreso Exitoso");
	tramitemunicipal.TramitesDAO.save(lormTramite);
	t.commit();
	return "ingreso existoso";
	}
	catch (Exception e) {
	t.rollback();
	return "Error-Rollback";
	}
	} catch (PersistentException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
	return "Error persistencia";
	}
	}
	/**
	* Retorna un listado de objeto de la clase Tramite
	* @return List<domain.Tramite>
	*/
	public List<domain.Tramite> mostrarTramite(){
	List<domain.Tramite> tramites= new	ArrayList<domain.Tramite>();
	tramitemunicipal.Tramites[] tramitesmuni;
	try {
	tramitesmuni = tramitemunicipal.TramitesDAO.listTramitesByQuery(null, null);
	int length = tramitesmuni.length;
	for (int i = 0; i < length; i++) {
	System.out.println(tramitesmuni[i]);
	tramites.add(new domain.Tramite(tramitesmuni[i].getCodigoformulario(),
			tramitesmuni[i].getFechaemision(),
			tramitesmuni[i].getCiudad(),
			tramitesmuni[i].getTipo(),
			tramitesmuni[i].getDescripcion(),
			tramitesmuni[i].getNombremunicipalidad(),
			tramitesmuni[i].getNombreusuario(),
			tramitesmuni[i].getApellidousuario(), 
			tramitesmuni[i].getRutusuario(),
			tramitesmuni[i].getRutfuncionario(),
			tramitesmuni[i].getCargofuncionario()));
	}
	return tramites;
	} catch (PersistentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	return null;
	}

	}
	/**
	* EliminaTramite
	* @param tramiteU
	* @return
	*/
	public String eliminarTramite(int codigoformulario) {
		PersistentTransaction t;
		try {
			t = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession()
					.beginTransaction();
			try {tramitemunicipal.Tramites lormTramite[] = tramitemunicipal.TramitesDAO.listTramitesByQuery(null, null);
			for (int i = 0; i < lormTramite.length; i++) {
				if(lormTramite[i].getCodigoformulario()==codigoformulario ) {
					 
						tramitemunicipal.TramitesDAO.delete(lormTramite[i]);
						t.commit();
						System.out.println("Eliminacion exitosa");
						return "Eliminacion existosa";
						}
			}
				
			
			return "No se encontro formulario";
			} catch (Exception e) {
				t.rollback();
				return "Error-Rollback";
			}
		} catch (PersistentException e1) {
			e1.printStackTrace();
			return "Error persistencia";
		}
		
	}
	/**
	* ModificaTramite
	* @param tramiteU
	* @param tramiteUVONuevo
	* @return
	*/
	public String modificarTramite(int codigo, domain.Tramite tramiteUNuevo) {
		PersistentTransaction t;
		try {
			t =tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession()
					.beginTransaction();
			try {
				
				tramitemunicipal.Tramites lormTramite[] = tramitemunicipal.TramitesDAO.listTramitesByQuery(null, null);
				for (int i = 0; i < lormTramite.length; i++) {
					if(lormTramite[i].getCodigoformulario()==codigo ) {
						    lormTramite[i].setNombremunicipalidad(tramiteUNuevo.getNombremunicipalidad());
							lormTramite[i].setCiudad(tramiteUNuevo.getCiudad());
							lormTramite[i].setFechaemision(tramiteUNuevo.getFechaemision());
							lormTramite[i].setRutfuncionario(tramiteUNuevo.getRutfuncionario());
							lormTramite[i].setCargofuncionario(tramiteUNuevo.getCargofuncionario());
							lormTramite[i].setTipo(tramiteUNuevo.getTipo());
							lormTramite[i].setNombreusuario(tramiteUNuevo.getNombreusuario());
							lormTramite[i].setApellidousuario(tramiteUNuevo.getApellidousuario());
							lormTramite[i].setRutusuario(tramiteUNuevo.getRutusuario());
							lormTramite[i].setDescripcion(tramiteUNuevo.getDescripcion());
							tramitemunicipal.TramitesDAO.save(lormTramite[i]);
							t.commit();
					}else{return "tramite no encontrado";}
				   
					
				}
				
				
				
				
				System.out.println("Modificacion exitosa");
				return "Modificacion existosa";
			} catch (Exception e) {
				t.rollback();
				return "Error-Rollback";
			}
		} catch (PersistentException e1) {
			e1.printStackTrace();
			return "Error persistencia";
		}
		
		
}
	/**
	* Retorna un listado de objeto de la clase Tramite
	* @param rutusuario
	* @return List<domain.Tramite>
	*/
	public List<domain.Tramite> buscarTramite(String rutusuario){
	List<domain.Tramite> tramites= new	ArrayList<domain.Tramite>();
	tramitemunicipal.Tramites[] tramitesmuni;
	try {
	tramitesmuni = tramitemunicipal.TramitesDAO.listTramitesByQuery(null, null);
	int length = tramitesmuni.length;
	for (int i = 0; i < length; i++) {
	if (tramitesmuni[i].getRutusuario().equals(rutusuario)) {
		System.out.println(tramitesmuni[i]);
		tramites.add(new domain.Tramite(tramitesmuni[i].getCodigoformulario(),
				tramitesmuni[i].getFechaemision(),
				tramitesmuni[i].getCiudad(),
				tramitesmuni[i].getTipo(),
				tramitesmuni[i].getDescripcion(),
				tramitesmuni[i].getNombremunicipalidad(),
				tramitesmuni[i].getNombreusuario(),
				tramitesmuni[i].getApellidousuario(), 
				tramitesmuni[i].getRutusuario(),
				tramitesmuni[i].getRutfuncionario(),
				tramitesmuni[i].getCargofuncionario()));	
	}
		
	}
	
	
	return tramites;
	} catch (PersistentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	return null;
	}

	}
	}
