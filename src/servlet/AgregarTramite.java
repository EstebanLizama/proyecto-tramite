package servlet;
import  webservice.*;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import domain.*;
/**
 * Servlet implementation class AgregarTramite
 */
public class AgregarTramite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregarTramite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * Permite traer los datos del jsp asignado AgregarTramite y conectarce con la capa de persistencia 
	 * @param request 
	 * @param response
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		ServicioTramite s=new ServicioTramite();
		Tramite t=new Tramite();
		t.setNombremunicipalidad(request.getParameter("nombremunicipalidad"));
		t.setCiudad(request.getParameter("ciudad"));
		t.setFechaemision(request.getParameter("fechaemision"));
		t.setRutfuncionario(request.getParameter("rutfuncionario"));
		t.setCargofuncionario(request.getParameter("cargofuncionario"));
		t.setTipo(request.getParameter("tipo"));
		t.setNombreusuario(request.getParameter("nombreusuario"));
		t.setApellidousuario(request.getParameter("apellidousuario"));
		t.setRutusuario(request.getParameter("rutusuario"));
		t.setDescripcion(request.getParameter("descripcion"));
		s.agregarTramite(t);
		request.setAttribute("out",("Ingreso exitoso"));
		request.getRequestDispatcher("/AgregarTramite.jsp").forward(request, response);
		doGet(request, response);
		
	}

}
