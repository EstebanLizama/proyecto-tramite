package servlet;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import webservice.*;
/**
 * Servlet implementation class EliminarTramite
 */
public class EliminarTramite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EliminarTramite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 *  Permite traer los datos del jsp asignado EliminarTramite y conectarce con la capa de persistencia 
	 * @param request 
	 * @param response
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String cod=request.getParameter("codigoformulario");
		 ServicioTramite s=new ServicioTramite();
		 int out1=Integer.parseInt(cod);
		
		request.setAttribute("out",(s.eliminarTramite(out1)));
		request.getRequestDispatcher("/EliminarTramite.jsp").forward(request, response);
		
		doGet(request, response);
	}

}
