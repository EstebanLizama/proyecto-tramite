package servlet;
import domain.*;
import webservice.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ModificarTramite
 */
public class ModificarTramite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarTramite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	*  Permite traer los datos del jsp asignado ModificarTramite y conectarce con la capa de persistencia 
	 * @param request 
	 * @param response 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out =response.getWriter();
		Tramite t=new Tramite();
		Tramite t2=new Tramite();
		ServicioTramite s=new ServicioTramite();
		String cod= request.getParameter("codigonuevo");
	   int cod1 =Integer.parseInt(cod);
		
		t2.setNombremunicipalidad(request.getParameter("nombremunicipalidad"));
		t2.setCiudad(request.getParameter("ciudad"));
		t2.setFechaemision(request.getParameter("fechaemision"));
		t2.setRutfuncionario(request.getParameter("rutfuncionario"));
		t2.setCargofuncionario(request.getParameter("cargofuncionario"));
		t2.setTipo(request.getParameter("tipo"));
		t2.setNombreusuario(request.getParameter("nombreusuario"));
		t2.setApellidousuario(request.getParameter("apellidousuario"));
		t2.setRutusuario(request.getParameter("rutusuario"));
		t2.setDescripcion(request.getParameter("descripcion"));
		s.modificarTramite(cod1,t2);
		request.setAttribute("out",("modificado exitoso"));
		request.getRequestDispatcher("/ModificarTramite.jsp").forward(request, response);
		
		doGet(request, response);
	}

}
