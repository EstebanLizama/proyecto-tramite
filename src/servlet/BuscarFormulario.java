package servlet;
import webservice.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BuscarFormulario
 */
public class BuscarFormulario extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarFormulario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * * Permite traer los datos del jsp asignado BuscarFormulario y conectarce con la capa de persistencia 
	 * @param request 
	 * @param response
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServicioTramite s=new ServicioTramite();
		String rutusariobuscar=request.getParameter("rutusuariobuscar");
		
		request.setAttribute("listaTramite",s.buscarTramite(rutusariobuscar));
		request.setAttribute("terminado", "La busqueda termino");
		request.getRequestDispatcher("/BuscarTramites.jsp").forward(request, response);
		
		doGet(request, response);
	}

}
