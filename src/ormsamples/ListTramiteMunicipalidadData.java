/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class ListTramiteMunicipalidadData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Tramites...");
		tramitemunicipal.Tramites[] tramiteMunicipalTramiteses = tramitemunicipal.TramitesDAO.listTramitesByQuery(null, null);
		int length = Math.min(tramiteMunicipalTramiteses.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(tramiteMunicipalTramiteses[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing Tramites by Criteria...");
		tramitemunicipal.TramitesCriteria tramiteMunicipalTramitesCriteria = new tramitemunicipal.TramitesCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//tramiteMunicipalTramitesCriteria.codigoformulario.eq();
		tramiteMunicipalTramitesCriteria.setMaxResults(ROW_COUNT);
		tramitemunicipal.Tramites[] tramiteMunicipalTramiteses = tramiteMunicipalTramitesCriteria.listTramites();
		int length =tramiteMunicipalTramiteses== null ? 0 : Math.min(tramiteMunicipalTramiteses.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(tramiteMunicipalTramiteses[i]);
		}
		System.out.println(length + " Tramites record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListTramiteMunicipalidadData listTramiteMunicipalidadData = new ListTramiteMunicipalidadData();
			try {
				listTramiteMunicipalidadData.listTestData();
				//listTramiteMunicipalidadData.listByCriteria();
			}
			finally {
				tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
