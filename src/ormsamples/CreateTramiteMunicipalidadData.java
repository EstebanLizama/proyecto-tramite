/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class CreateTramiteMunicipalidadData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession().beginTransaction();
		try {
			tramitemunicipal.Tramites tramiteMunicipalTramites = tramitemunicipal.TramitesDAO.createTramites();
			// Initialize the properties of the persistent object here
			tramitemunicipal.TramitesDAO.save(tramiteMunicipalTramites);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateTramiteMunicipalidadData createTramiteMunicipalidadData = new CreateTramiteMunicipalidadData();
			try {
				createTramiteMunicipalidadData.createTestData();
			}
			finally {
				tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
