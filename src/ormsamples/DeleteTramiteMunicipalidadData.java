/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class DeleteTramiteMunicipalidadData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession().beginTransaction();
		try {
			tramitemunicipal.Tramites tramiteMunicipalTramites = tramitemunicipal.TramitesDAO.loadTramitesByQuery(null, null);
			// Delete the persistent object
			tramitemunicipal.TramitesDAO.delete(tramiteMunicipalTramites);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteTramiteMunicipalidadData deleteTramiteMunicipalidadData = new DeleteTramiteMunicipalidadData();
			try {
				deleteTramiteMunicipalidadData.deleteTestData();
			}
			finally {
				tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
