/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateTramiteMunicipalidadData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().getSession().beginTransaction();
		try {
			tramitemunicipal.Tramites tramiteMunicipalTramites = tramitemunicipal.TramitesDAO.loadTramitesByQuery(null, null);
			// Update the properties of the persistent object
			tramitemunicipal.TramitesDAO.save(tramiteMunicipalTramites);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Tramites by TramitesCriteria");
		tramitemunicipal.TramitesCriteria tramiteMunicipalTramitesCriteria = new tramitemunicipal.TramitesCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//tramiteMunicipalTramitesCriteria.codigoformulario.eq();
		System.out.println(tramiteMunicipalTramitesCriteria.uniqueTramites());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateTramiteMunicipalidadData retrieveAndUpdateTramiteMunicipalidadData = new RetrieveAndUpdateTramiteMunicipalidadData();
			try {
				retrieveAndUpdateTramiteMunicipalidadData.retrieveAndUpdateTestData();
				//retrieveAndUpdateTramiteMunicipalidadData.retrieveByCriteria();
			}
			finally {
				tramitemunicipal.TramiteMunicipalidadPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
